import os
from typing import Optional
from threading import RLock, Thread, Event
from queue import SimpleQueue
import rclpy
import rclpy.task
import rclpy.action
import rclpy.executors
from rclpy.callback_groups import MutuallyExclusiveCallbackGroup
from rclpy.executors import SingleThreadedExecutor

from turtle_managers.action import DiveSkill

from turtle_managers.action import AscendSkill

from turtle_managers.action import MoveOnSurfaceSkill

from turtle_managers.action import MoveSkill

from turtle_managers.action import TurnSensorOnSkill

from turtle_managers.action import TurnSensorOffSkill

from turtle_managers.action import SearchSkill

from .logic import *
from .resourceset_interface import ResourceSet


class DiveSkillInterface:
    def __init__(self, **kwargs):
        self.__name = 'dive'
        self.__node = rclpy.create_node(f"turtle_interface_{self.__name}_{os.getpid()}", use_global_arguments=False, **kwargs)
        self.__executor = SingleThreadedExecutor()
        self.__executor.add_node(self.__node)
        self.__queue = SimpleQueue()
        self.__running = Event()
        self.__running.set()
        self.__rlock = RLock()
        self.__thread = Thread(target=self.__spin)
        self.__thread.start()

        self.__state = None
        self.__progress = None
        # formula for preconditions
        self.__formula_pre = dict()
        
        self.__formula_pre['on_surface_ready'] = And(Equals('turtle_status', 'ON_SURFACE'), Equals('control_mode', 'IDLE'))
        
        self.__formula_pre['autonomous'] = Equals('autonomous_control', 'ON')
        
        self.__client = rclpy.action.client.ActionClient(self.__node, DiveSkill, 
                    f"skill/{ self.__name }", callback_group=MutuallyExclusiveCallbackGroup())
        self.__action_spec = DiveSkill
        self.__gh = None
        self.__send_goal_future = None
        self.__get_result_future = None
        self.__cancel_goal_future = None
        
    def __del__(self):
        self.__executor.shutdown()
        self.__thread.join()

    def __spin(self):
        while rclpy.ok():
            try:
                self.__executor.spin_once(timeout_sec=0.1)
            except Exception as e:
                return
    
    @property
    def name(self):
        """Skill name"""
        return self.__name

    ## Server connections
    @property
    def ready(self) -> bool:
        return self.__client.server_is_ready()

    def __nonzero__(self):
        return self.ready

    def __bool__(self):
        return self.ready

    def wait_manager(self, timeout: Optional[float] = None) -> bool:
        self.__node.get_logger().debug(f"Skill {self.__name} wait for manager server")
        return self.__client.wait_for_server(timeout_sec=timeout)

    ## Progress
    def __feedback_cb(self, feedback_msg):
        self.__node.get_logger().debug(f"received feedback {feedback_msg.feedback}")
        with self.__rlock:
            self.__progress = feedback_msg.feedback.progress

    @property
    def progress(self) -> float:
        """Progress value"""
        return self.__progress

    ## Preconditions
    def check_resource_precondition(self, resources: ResourceSet):
        for label, formula in self.__formula_pre.items():
            states = dict()
            for res in formula.symbols():
                states[res] = resources[res].get()
            if not formula.eval(states):
                return False
        return True

    ## Start
    def __goal_cb(self, future):
        self.__node.get_logger().debug("goal response received")
        self.__queue.put(future.result())

    def start(self, **kwargs) -> Optional[rclpy.task.Future] :
        """Start the skill execution.
        """
        with self.__rlock:
            self.__progress = 0.0
            self.__send_goal_future = self.__client.send_goal_async(self.__action_spec.Goal(**kwargs),
                                                                    feedback_callback = self.__feedback_cb)
            self.__send_goal_future.add_done_callback(self.__goal_cb)
            self.__gh = self.__queue.get()
            if not self.__gh.accepted:
                self.__node.get_logger().warning(f"Skill {self.__name} execution rejected")
                self.__state = 'NV'
                return None

            self.__node.get_logger().debug(f"Skill {self.__name} execution accepted")
            self.__running.clear()
            self.__get_result_future = self.__gh.get_result_async()
            self.__get_result_future.add_done_callback(self.__done_cb)
            self.__state = 'RG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return self.__get_result_future

    def __done_cb(self, future):
        self.__node.get_logger().debug("future done")
        with self.__rlock:
            result = future.result()
            self.__state = result.result.mode
            self.__node.get_logger().debug(f"Received result {self.__state}")
            self.__running.set()
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            
    def wait(self, timeout: Optional[float] = None) -> bool:
        """Wait for completion"""
        self.__node.get_logger().debug(f"Skill {self.__name} running? {self.running}")
        self.__node.get_logger().debug(f"{self.__name} wait until future complete")
        return self.__running.wait(timeout=timeout)

    @property
    def running(self) -> bool:
        return not self.__running.is_set()

    ## Mode
    @property
    def mode(self) -> str:
        """Final mode"""
        return self.__state

    ## Interruption
    def interrupt(self):
        """Interrupt execution"""
        if self.running and self.__gh is not None:
            self.__cancel_goal_future = self.__gh.cancel_goal_async()
            self.__cancel_goal_future.add_done_callback(self.__goal_cb)
            result = self.__queue.get()
            if result.return_code != 0:
                self.__node.get_logger().warning(f"Skill {self.__name} interruption rejected")
                self.__state = 'NV'
                return False

            self.__node.get_logger().debug(f"Skill {self.__name} interruption accepted")
            self.__cancel_result_future = self.__gh.get_result_async()
            self.__cancel_result_future.add_done_callback(self.__done_cb)
            self.__state = 'IG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return True
        return True

class AscendSkillInterface:
    def __init__(self, **kwargs):
        self.__name = 'ascend'
        self.__node = rclpy.create_node(f"turtle_interface_{self.__name}_{os.getpid()}", use_global_arguments=False, **kwargs)
        self.__executor = SingleThreadedExecutor()
        self.__executor.add_node(self.__node)
        self.__queue = SimpleQueue()
        self.__running = Event()
        self.__running.set()
        self.__rlock = RLock()
        self.__thread = Thread(target=self.__spin)
        self.__thread.start()

        self.__state = None
        self.__progress = None
        # formula for preconditions
        self.__formula_pre = dict()
        
        self.__formula_pre['in_water_ready'] = And(Equals('turtle_status', 'IN_WATER'), Equals('control_mode', 'IDLE'))
        
        self.__formula_pre['autonomous'] = Equals('autonomous_control', 'ON')
        
        self.__client = rclpy.action.client.ActionClient(self.__node, AscendSkill, 
                    f"skill/{ self.__name }", callback_group=MutuallyExclusiveCallbackGroup())
        self.__action_spec = AscendSkill
        self.__gh = None
        self.__send_goal_future = None
        self.__get_result_future = None
        self.__cancel_goal_future = None
        
    def __del__(self):
        self.__executor.shutdown()
        self.__thread.join()

    def __spin(self):
        while rclpy.ok():
            try:
                self.__executor.spin_once(timeout_sec=0.1)
            except Exception as e:
                return
    
    @property
    def name(self):
        """Skill name"""
        return self.__name

    ## Server connections
    @property
    def ready(self) -> bool:
        return self.__client.server_is_ready()

    def __nonzero__(self):
        return self.ready

    def __bool__(self):
        return self.ready

    def wait_manager(self, timeout: Optional[float] = None) -> bool:
        self.__node.get_logger().debug(f"Skill {self.__name} wait for manager server")
        return self.__client.wait_for_server(timeout_sec=timeout)

    ## Progress
    def __feedback_cb(self, feedback_msg):
        self.__node.get_logger().debug(f"received feedback {feedback_msg.feedback}")
        with self.__rlock:
            self.__progress = feedback_msg.feedback.progress

    @property
    def progress(self) -> float:
        """Progress value"""
        return self.__progress

    ## Preconditions
    def check_resource_precondition(self, resources: ResourceSet):
        for label, formula in self.__formula_pre.items():
            states = dict()
            for res in formula.symbols():
                states[res] = resources[res].get()
            if not formula.eval(states):
                return False
        return True

    ## Start
    def __goal_cb(self, future):
        self.__node.get_logger().debug("goal response received")
        self.__queue.put(future.result())

    def start(self, **kwargs) -> Optional[rclpy.task.Future] :
        """Start the skill execution.
        """
        with self.__rlock:
            self.__progress = 0.0
            self.__send_goal_future = self.__client.send_goal_async(self.__action_spec.Goal(**kwargs),
                                                                    feedback_callback = self.__feedback_cb)
            self.__send_goal_future.add_done_callback(self.__goal_cb)
            self.__gh = self.__queue.get()
            if not self.__gh.accepted:
                self.__node.get_logger().warning(f"Skill {self.__name} execution rejected")
                self.__state = 'NV'
                return None

            self.__node.get_logger().debug(f"Skill {self.__name} execution accepted")
            self.__running.clear()
            self.__get_result_future = self.__gh.get_result_async()
            self.__get_result_future.add_done_callback(self.__done_cb)
            self.__state = 'RG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return self.__get_result_future

    def __done_cb(self, future):
        self.__node.get_logger().debug("future done")
        with self.__rlock:
            result = future.result()
            self.__state = result.result.mode
            self.__node.get_logger().debug(f"Received result {self.__state}")
            self.__running.set()
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            
    def wait(self, timeout: Optional[float] = None) -> bool:
        """Wait for completion"""
        self.__node.get_logger().debug(f"Skill {self.__name} running? {self.running}")
        self.__node.get_logger().debug(f"{self.__name} wait until future complete")
        return self.__running.wait(timeout=timeout)

    @property
    def running(self) -> bool:
        return not self.__running.is_set()

    ## Mode
    @property
    def mode(self) -> str:
        """Final mode"""
        return self.__state

    ## Interruption
    def interrupt(self):
        """Interrupt execution"""
        if self.running and self.__gh is not None:
            self.__cancel_goal_future = self.__gh.cancel_goal_async()
            self.__cancel_goal_future.add_done_callback(self.__goal_cb)
            result = self.__queue.get()
            if result.return_code != 0:
                self.__node.get_logger().warning(f"Skill {self.__name} interruption rejected")
                self.__state = 'NV'
                return False

            self.__node.get_logger().debug(f"Skill {self.__name} interruption accepted")
            self.__cancel_result_future = self.__gh.get_result_async()
            self.__cancel_result_future.add_done_callback(self.__done_cb)
            self.__state = 'IG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return True
        return True

class MoveOnSurfaceSkillInterface:
    def __init__(self, **kwargs):
        self.__name = 'move_on_surface'
        self.__node = rclpy.create_node(f"turtle_interface_{self.__name}_{os.getpid()}", use_global_arguments=False, **kwargs)
        self.__executor = SingleThreadedExecutor()
        self.__executor.add_node(self.__node)
        self.__queue = SimpleQueue()
        self.__running = Event()
        self.__running.set()
        self.__rlock = RLock()
        self.__thread = Thread(target=self.__spin)
        self.__thread.start()

        self.__state = None
        self.__progress = None
        # formula for preconditions
        self.__formula_pre = dict()
        
        self.__formula_pre['on_surface'] = And(Equals('turtle_status', 'ON_SURFACE'), Equals('control_mode', 'IDLE'))
        
        self.__formula_pre['autonomous'] = Equals('autonomous_control', 'ON')
        
        self.__client = rclpy.action.client.ActionClient(self.__node, MoveOnSurfaceSkill, 
                    f"skill/{ self.__name }", callback_group=MutuallyExclusiveCallbackGroup())
        self.__action_spec = MoveOnSurfaceSkill
        self.__gh = None
        self.__send_goal_future = None
        self.__get_result_future = None
        self.__cancel_goal_future = None
        
    def __del__(self):
        self.__executor.shutdown()
        self.__thread.join()

    def __spin(self):
        while rclpy.ok():
            try:
                self.__executor.spin_once(timeout_sec=0.1)
            except Exception as e:
                return
    
    @property
    def name(self):
        """Skill name"""
        return self.__name

    ## Server connections
    @property
    def ready(self) -> bool:
        return self.__client.server_is_ready()

    def __nonzero__(self):
        return self.ready

    def __bool__(self):
        return self.ready

    def wait_manager(self, timeout: Optional[float] = None) -> bool:
        self.__node.get_logger().debug(f"Skill {self.__name} wait for manager server")
        return self.__client.wait_for_server(timeout_sec=timeout)

    ## Progress
    def __feedback_cb(self, feedback_msg):
        self.__node.get_logger().debug(f"received feedback {feedback_msg.feedback}")
        with self.__rlock:
            self.__progress = feedback_msg.feedback.progress

    @property
    def progress(self) -> float:
        """Progress value"""
        return self.__progress

    ## Preconditions
    def check_resource_precondition(self, resources: ResourceSet):
        for label, formula in self.__formula_pre.items():
            states = dict()
            for res in formula.symbols():
                states[res] = resources[res].get()
            if not formula.eval(states):
                return False
        return True

    ## Start
    def __goal_cb(self, future):
        self.__node.get_logger().debug("goal response received")
        self.__queue.put(future.result())

    def start(self, **kwargs) -> Optional[rclpy.task.Future] :
        """Start the skill execution.
        """
        with self.__rlock:
            self.__progress = 0.0
            self.__send_goal_future = self.__client.send_goal_async(self.__action_spec.Goal(**kwargs),
                                                                    feedback_callback = self.__feedback_cb)
            self.__send_goal_future.add_done_callback(self.__goal_cb)
            self.__gh = self.__queue.get()
            if not self.__gh.accepted:
                self.__node.get_logger().warning(f"Skill {self.__name} execution rejected")
                self.__state = 'NV'
                return None

            self.__node.get_logger().debug(f"Skill {self.__name} execution accepted")
            self.__running.clear()
            self.__get_result_future = self.__gh.get_result_async()
            self.__get_result_future.add_done_callback(self.__done_cb)
            self.__state = 'RG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return self.__get_result_future

    def __done_cb(self, future):
        self.__node.get_logger().debug("future done")
        with self.__rlock:
            result = future.result()
            self.__state = result.result.mode
            self.__node.get_logger().debug(f"Received result {self.__state}")
            self.__running.set()
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            
    def wait(self, timeout: Optional[float] = None) -> bool:
        """Wait for completion"""
        self.__node.get_logger().debug(f"Skill {self.__name} running? {self.running}")
        self.__node.get_logger().debug(f"{self.__name} wait until future complete")
        return self.__running.wait(timeout=timeout)

    @property
    def running(self) -> bool:
        return not self.__running.is_set()

    ## Mode
    @property
    def mode(self) -> str:
        """Final mode"""
        return self.__state

    ## Interruption
    def interrupt(self):
        """Interrupt execution"""
        if self.running and self.__gh is not None:
            self.__cancel_goal_future = self.__gh.cancel_goal_async()
            self.__cancel_goal_future.add_done_callback(self.__goal_cb)
            result = self.__queue.get()
            if result.return_code != 0:
                self.__node.get_logger().warning(f"Skill {self.__name} interruption rejected")
                self.__state = 'NV'
                return False

            self.__node.get_logger().debug(f"Skill {self.__name} interruption accepted")
            self.__cancel_result_future = self.__gh.get_result_async()
            self.__cancel_result_future.add_done_callback(self.__done_cb)
            self.__state = 'IG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return True
        return True

class MoveSkillInterface:
    def __init__(self, **kwargs):
        self.__name = 'move'
        self.__node = rclpy.create_node(f"turtle_interface_{self.__name}_{os.getpid()}", use_global_arguments=False, **kwargs)
        self.__executor = SingleThreadedExecutor()
        self.__executor.add_node(self.__node)
        self.__queue = SimpleQueue()
        self.__running = Event()
        self.__running.set()
        self.__rlock = RLock()
        self.__thread = Thread(target=self.__spin)
        self.__thread.start()

        self.__state = None
        self.__progress = None
        # formula for preconditions
        self.__formula_pre = dict()
        
        self.__formula_pre['in_water'] = And(Equals('turtle_status', 'IN_WATER'), Equals('control_mode', 'IDLE'))
        
        self.__formula_pre['autonomous'] = Equals('autonomous_control', 'ON')
        
        self.__client = rclpy.action.client.ActionClient(self.__node, MoveSkill, 
                    f"skill/{ self.__name }", callback_group=MutuallyExclusiveCallbackGroup())
        self.__action_spec = MoveSkill
        self.__gh = None
        self.__send_goal_future = None
        self.__get_result_future = None
        self.__cancel_goal_future = None
        
    def __del__(self):
        self.__executor.shutdown()
        self.__thread.join()

    def __spin(self):
        while rclpy.ok():
            try:
                self.__executor.spin_once(timeout_sec=0.1)
            except Exception as e:
                return
    
    @property
    def name(self):
        """Skill name"""
        return self.__name

    ## Server connections
    @property
    def ready(self) -> bool:
        return self.__client.server_is_ready()

    def __nonzero__(self):
        return self.ready

    def __bool__(self):
        return self.ready

    def wait_manager(self, timeout: Optional[float] = None) -> bool:
        self.__node.get_logger().debug(f"Skill {self.__name} wait for manager server")
        return self.__client.wait_for_server(timeout_sec=timeout)

    ## Progress
    def __feedback_cb(self, feedback_msg):
        self.__node.get_logger().debug(f"received feedback {feedback_msg.feedback}")
        with self.__rlock:
            self.__progress = feedback_msg.feedback.progress

    @property
    def progress(self) -> float:
        """Progress value"""
        return self.__progress

    ## Preconditions
    def check_resource_precondition(self, resources: ResourceSet):
        for label, formula in self.__formula_pre.items():
            states = dict()
            for res in formula.symbols():
                states[res] = resources[res].get()
            if not formula.eval(states):
                return False
        return True

    ## Start
    def __goal_cb(self, future):
        self.__node.get_logger().debug("goal response received")
        self.__queue.put(future.result())

    def start(self, **kwargs) -> Optional[rclpy.task.Future] :
        """Start the skill execution.
        """
        with self.__rlock:
            self.__progress = 0.0
            self.__send_goal_future = self.__client.send_goal_async(self.__action_spec.Goal(**kwargs),
                                                                    feedback_callback = self.__feedback_cb)
            self.__send_goal_future.add_done_callback(self.__goal_cb)
            self.__gh = self.__queue.get()
            if not self.__gh.accepted:
                self.__node.get_logger().warning(f"Skill {self.__name} execution rejected")
                self.__state = 'NV'
                return None

            self.__node.get_logger().debug(f"Skill {self.__name} execution accepted")
            self.__running.clear()
            self.__get_result_future = self.__gh.get_result_async()
            self.__get_result_future.add_done_callback(self.__done_cb)
            self.__state = 'RG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return self.__get_result_future

    def __done_cb(self, future):
        self.__node.get_logger().debug("future done")
        with self.__rlock:
            result = future.result()
            self.__state = result.result.mode
            self.__node.get_logger().debug(f"Received result {self.__state}")
            self.__running.set()
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            
    def wait(self, timeout: Optional[float] = None) -> bool:
        """Wait for completion"""
        self.__node.get_logger().debug(f"Skill {self.__name} running? {self.running}")
        self.__node.get_logger().debug(f"{self.__name} wait until future complete")
        return self.__running.wait(timeout=timeout)

    @property
    def running(self) -> bool:
        return not self.__running.is_set()

    ## Mode
    @property
    def mode(self) -> str:
        """Final mode"""
        return self.__state

    ## Interruption
    def interrupt(self):
        """Interrupt execution"""
        if self.running and self.__gh is not None:
            self.__cancel_goal_future = self.__gh.cancel_goal_async()
            self.__cancel_goal_future.add_done_callback(self.__goal_cb)
            result = self.__queue.get()
            if result.return_code != 0:
                self.__node.get_logger().warning(f"Skill {self.__name} interruption rejected")
                self.__state = 'NV'
                return False

            self.__node.get_logger().debug(f"Skill {self.__name} interruption accepted")
            self.__cancel_result_future = self.__gh.get_result_async()
            self.__cancel_result_future.add_done_callback(self.__done_cb)
            self.__state = 'IG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return True
        return True

class TurnSensorOnSkillInterface:
    def __init__(self, **kwargs):
        self.__name = 'turn_sensor_on'
        self.__node = rclpy.create_node(f"turtle_interface_{self.__name}_{os.getpid()}", use_global_arguments=False, **kwargs)
        self.__executor = SingleThreadedExecutor()
        self.__executor.add_node(self.__node)
        self.__queue = SimpleQueue()
        self.__running = Event()
        self.__running.set()
        self.__rlock = RLock()
        self.__thread = Thread(target=self.__spin)
        self.__thread.start()

        self.__state = None
        self.__progress = None
        # formula for preconditions
        self.__formula_pre = dict()
        
        self.__formula_pre['sensor_off'] = Equals('sensor', 'OFF')
        
        self.__client = rclpy.action.client.ActionClient(self.__node, TurnSensorOnSkill, 
                    f"skill/{ self.__name }", callback_group=MutuallyExclusiveCallbackGroup())
        self.__action_spec = TurnSensorOnSkill
        self.__gh = None
        self.__send_goal_future = None
        self.__get_result_future = None
        self.__cancel_goal_future = None
        
    def __del__(self):
        self.__executor.shutdown()
        self.__thread.join()

    def __spin(self):
        while rclpy.ok():
            try:
                self.__executor.spin_once(timeout_sec=0.1)
            except Exception as e:
                return
    
    @property
    def name(self):
        """Skill name"""
        return self.__name

    ## Server connections
    @property
    def ready(self) -> bool:
        return self.__client.server_is_ready()

    def __nonzero__(self):
        return self.ready

    def __bool__(self):
        return self.ready

    def wait_manager(self, timeout: Optional[float] = None) -> bool:
        self.__node.get_logger().debug(f"Skill {self.__name} wait for manager server")
        return self.__client.wait_for_server(timeout_sec=timeout)

    ## Progress
    def __feedback_cb(self, feedback_msg):
        self.__node.get_logger().debug(f"received feedback {feedback_msg.feedback}")
        with self.__rlock:
            self.__progress = feedback_msg.feedback.progress

    @property
    def progress(self) -> float:
        """Progress value"""
        return self.__progress

    ## Preconditions
    def check_resource_precondition(self, resources: ResourceSet):
        for label, formula in self.__formula_pre.items():
            states = dict()
            for res in formula.symbols():
                states[res] = resources[res].get()
            if not formula.eval(states):
                return False
        return True

    ## Start
    def __goal_cb(self, future):
        self.__node.get_logger().debug("goal response received")
        self.__queue.put(future.result())

    def start(self, **kwargs) -> Optional[rclpy.task.Future] :
        """Start the skill execution.
        """
        with self.__rlock:
            self.__progress = 0.0
            self.__send_goal_future = self.__client.send_goal_async(self.__action_spec.Goal(**kwargs),
                                                                    feedback_callback = self.__feedback_cb)
            self.__send_goal_future.add_done_callback(self.__goal_cb)
            self.__gh = self.__queue.get()
            if not self.__gh.accepted:
                self.__node.get_logger().warning(f"Skill {self.__name} execution rejected")
                self.__state = 'NV'
                return None

            self.__node.get_logger().debug(f"Skill {self.__name} execution accepted")
            self.__running.clear()
            self.__get_result_future = self.__gh.get_result_async()
            self.__get_result_future.add_done_callback(self.__done_cb)
            self.__state = 'RG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return self.__get_result_future

    def __done_cb(self, future):
        self.__node.get_logger().debug("future done")
        with self.__rlock:
            result = future.result()
            self.__state = result.result.mode
            self.__node.get_logger().debug(f"Received result {self.__state}")
            self.__running.set()
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            
    def wait(self, timeout: Optional[float] = None) -> bool:
        """Wait for completion"""
        self.__node.get_logger().debug(f"Skill {self.__name} running? {self.running}")
        self.__node.get_logger().debug(f"{self.__name} wait until future complete")
        return self.__running.wait(timeout=timeout)

    @property
    def running(self) -> bool:
        return not self.__running.is_set()

    ## Mode
    @property
    def mode(self) -> str:
        """Final mode"""
        return self.__state

    ## Interruption
    def interrupt(self):
        """Interrupt execution"""
        if self.running and self.__gh is not None:
            self.__cancel_goal_future = self.__gh.cancel_goal_async()
            self.__cancel_goal_future.add_done_callback(self.__goal_cb)
            result = self.__queue.get()
            if result.return_code != 0:
                self.__node.get_logger().warning(f"Skill {self.__name} interruption rejected")
                self.__state = 'NV'
                return False

            self.__node.get_logger().debug(f"Skill {self.__name} interruption accepted")
            self.__cancel_result_future = self.__gh.get_result_async()
            self.__cancel_result_future.add_done_callback(self.__done_cb)
            self.__state = 'IG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return True
        return True

class TurnSensorOffSkillInterface:
    def __init__(self, **kwargs):
        self.__name = 'turn_sensor_off'
        self.__node = rclpy.create_node(f"turtle_interface_{self.__name}_{os.getpid()}", use_global_arguments=False, **kwargs)
        self.__executor = SingleThreadedExecutor()
        self.__executor.add_node(self.__node)
        self.__queue = SimpleQueue()
        self.__running = Event()
        self.__running.set()
        self.__rlock = RLock()
        self.__thread = Thread(target=self.__spin)
        self.__thread.start()

        self.__state = None
        self.__progress = None
        # formula for preconditions
        self.__formula_pre = dict()
        
        self.__formula_pre['sensor_on'] = Equals('sensor', 'ON')
        
        self.__client = rclpy.action.client.ActionClient(self.__node, TurnSensorOffSkill, 
                    f"skill/{ self.__name }", callback_group=MutuallyExclusiveCallbackGroup())
        self.__action_spec = TurnSensorOffSkill
        self.__gh = None
        self.__send_goal_future = None
        self.__get_result_future = None
        self.__cancel_goal_future = None
        
    def __del__(self):
        self.__executor.shutdown()
        self.__thread.join()

    def __spin(self):
        while rclpy.ok():
            try:
                self.__executor.spin_once(timeout_sec=0.1)
            except Exception as e:
                return
    
    @property
    def name(self):
        """Skill name"""
        return self.__name

    ## Server connections
    @property
    def ready(self) -> bool:
        return self.__client.server_is_ready()

    def __nonzero__(self):
        return self.ready

    def __bool__(self):
        return self.ready

    def wait_manager(self, timeout: Optional[float] = None) -> bool:
        self.__node.get_logger().debug(f"Skill {self.__name} wait for manager server")
        return self.__client.wait_for_server(timeout_sec=timeout)

    ## Progress
    def __feedback_cb(self, feedback_msg):
        self.__node.get_logger().debug(f"received feedback {feedback_msg.feedback}")
        with self.__rlock:
            self.__progress = feedback_msg.feedback.progress

    @property
    def progress(self) -> float:
        """Progress value"""
        return self.__progress

    ## Preconditions
    def check_resource_precondition(self, resources: ResourceSet):
        for label, formula in self.__formula_pre.items():
            states = dict()
            for res in formula.symbols():
                states[res] = resources[res].get()
            if not formula.eval(states):
                return False
        return True

    ## Start
    def __goal_cb(self, future):
        self.__node.get_logger().debug("goal response received")
        self.__queue.put(future.result())

    def start(self, **kwargs) -> Optional[rclpy.task.Future] :
        """Start the skill execution.
        """
        with self.__rlock:
            self.__progress = 0.0
            self.__send_goal_future = self.__client.send_goal_async(self.__action_spec.Goal(**kwargs),
                                                                    feedback_callback = self.__feedback_cb)
            self.__send_goal_future.add_done_callback(self.__goal_cb)
            self.__gh = self.__queue.get()
            if not self.__gh.accepted:
                self.__node.get_logger().warning(f"Skill {self.__name} execution rejected")
                self.__state = 'NV'
                return None

            self.__node.get_logger().debug(f"Skill {self.__name} execution accepted")
            self.__running.clear()
            self.__get_result_future = self.__gh.get_result_async()
            self.__get_result_future.add_done_callback(self.__done_cb)
            self.__state = 'RG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return self.__get_result_future

    def __done_cb(self, future):
        self.__node.get_logger().debug("future done")
        with self.__rlock:
            result = future.result()
            self.__state = result.result.mode
            self.__node.get_logger().debug(f"Received result {self.__state}")
            self.__running.set()
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            
    def wait(self, timeout: Optional[float] = None) -> bool:
        """Wait for completion"""
        self.__node.get_logger().debug(f"Skill {self.__name} running? {self.running}")
        self.__node.get_logger().debug(f"{self.__name} wait until future complete")
        return self.__running.wait(timeout=timeout)

    @property
    def running(self) -> bool:
        return not self.__running.is_set()

    ## Mode
    @property
    def mode(self) -> str:
        """Final mode"""
        return self.__state

    ## Interruption
    def interrupt(self):
        """Interrupt execution"""
        if self.running and self.__gh is not None:
            self.__cancel_goal_future = self.__gh.cancel_goal_async()
            self.__cancel_goal_future.add_done_callback(self.__goal_cb)
            result = self.__queue.get()
            if result.return_code != 0:
                self.__node.get_logger().warning(f"Skill {self.__name} interruption rejected")
                self.__state = 'NV'
                return False

            self.__node.get_logger().debug(f"Skill {self.__name} interruption accepted")
            self.__cancel_result_future = self.__gh.get_result_async()
            self.__cancel_result_future.add_done_callback(self.__done_cb)
            self.__state = 'IG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return True
        return True

class SearchSkillInterface:
    def __init__(self, **kwargs):
        self.__name = 'search'
        self.__node = rclpy.create_node(f"turtle_interface_{self.__name}_{os.getpid()}", use_global_arguments=False, **kwargs)
        self.__executor = SingleThreadedExecutor()
        self.__executor.add_node(self.__node)
        self.__queue = SimpleQueue()
        self.__running = Event()
        self.__running.set()
        self.__rlock = RLock()
        self.__thread = Thread(target=self.__spin)
        self.__thread.start()

        self.__state = None
        self.__progress = None
        # formula for preconditions
        self.__formula_pre = dict()
        
        self.__formula_pre['in_water'] = And(Equals('turtle_status', 'IN_WATER'), Equals('control_mode', 'IDLE'))
        
        self.__formula_pre['sensor_on'] = Equals('sensor', 'ON')
        
        self.__formula_pre['autonomous'] = Equals('autonomous_control', 'ON')
        
        self.__client = rclpy.action.client.ActionClient(self.__node, SearchSkill, 
                    f"skill/{ self.__name }", callback_group=MutuallyExclusiveCallbackGroup())
        self.__action_spec = SearchSkill
        self.__gh = None
        self.__send_goal_future = None
        self.__get_result_future = None
        self.__cancel_goal_future = None
        
    def __del__(self):
        self.__executor.shutdown()
        self.__thread.join()

    def __spin(self):
        while rclpy.ok():
            try:
                self.__executor.spin_once(timeout_sec=0.1)
            except Exception as e:
                return
    
    @property
    def name(self):
        """Skill name"""
        return self.__name

    ## Server connections
    @property
    def ready(self) -> bool:
        return self.__client.server_is_ready()

    def __nonzero__(self):
        return self.ready

    def __bool__(self):
        return self.ready

    def wait_manager(self, timeout: Optional[float] = None) -> bool:
        self.__node.get_logger().debug(f"Skill {self.__name} wait for manager server")
        return self.__client.wait_for_server(timeout_sec=timeout)

    ## Progress
    def __feedback_cb(self, feedback_msg):
        self.__node.get_logger().debug(f"received feedback {feedback_msg.feedback}")
        with self.__rlock:
            self.__progress = feedback_msg.feedback.progress

    @property
    def progress(self) -> float:
        """Progress value"""
        return self.__progress

    ## Preconditions
    def check_resource_precondition(self, resources: ResourceSet):
        for label, formula in self.__formula_pre.items():
            states = dict()
            for res in formula.symbols():
                states[res] = resources[res].get()
            if not formula.eval(states):
                return False
        return True

    ## Start
    def __goal_cb(self, future):
        self.__node.get_logger().debug("goal response received")
        self.__queue.put(future.result())

    def start(self, **kwargs) -> Optional[rclpy.task.Future] :
        """Start the skill execution.
        """
        with self.__rlock:
            self.__progress = 0.0
            self.__send_goal_future = self.__client.send_goal_async(self.__action_spec.Goal(**kwargs),
                                                                    feedback_callback = self.__feedback_cb)
            self.__send_goal_future.add_done_callback(self.__goal_cb)
            self.__gh = self.__queue.get()
            if not self.__gh.accepted:
                self.__node.get_logger().warning(f"Skill {self.__name} execution rejected")
                self.__state = 'NV'
                return None

            self.__node.get_logger().debug(f"Skill {self.__name} execution accepted")
            self.__running.clear()
            self.__get_result_future = self.__gh.get_result_async()
            self.__get_result_future.add_done_callback(self.__done_cb)
            self.__state = 'RG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return self.__get_result_future

    def __done_cb(self, future):
        self.__node.get_logger().debug("future done")
        with self.__rlock:
            result = future.result()
            self.__state = result.result.mode
            self.__node.get_logger().debug(f"Received result {self.__state}")
            self.__running.set()
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            
    def wait(self, timeout: Optional[float] = None) -> bool:
        """Wait for completion"""
        self.__node.get_logger().debug(f"Skill {self.__name} running? {self.running}")
        self.__node.get_logger().debug(f"{self.__name} wait until future complete")
        return self.__running.wait(timeout=timeout)

    @property
    def running(self) -> bool:
        return not self.__running.is_set()

    ## Mode
    @property
    def mode(self) -> str:
        """Final mode"""
        return self.__state

    ## Interruption
    def interrupt(self):
        """Interrupt execution"""
        if self.running and self.__gh is not None:
            self.__cancel_goal_future = self.__gh.cancel_goal_async()
            self.__cancel_goal_future.add_done_callback(self.__goal_cb)
            result = self.__queue.get()
            if result.return_code != 0:
                self.__node.get_logger().warning(f"Skill {self.__name} interruption rejected")
                self.__state = 'NV'
                return False

            self.__node.get_logger().debug(f"Skill {self.__name} interruption accepted")
            self.__cancel_result_future = self.__gh.get_result_async()
            self.__cancel_result_future.add_done_callback(self.__done_cb)
            self.__state = 'IG'
            self.__node.get_logger().debug(f"Skill {self.__name} running... {self.running}")
            return True
        return True

