import os
from typing import Optional
from threading import Thread
from queue import SimpleQueue
import rclpy
from rclpy.executors import SingleThreadedExecutor
from rclpy.callback_groups import MutuallyExclusiveCallbackGroup

from turtle_managers.srv import GetPositionData


class PositionData:
    def __init__(self, **kwargs):
        self.__name = 'position'
        self.__node = rclpy.create_node(f"turtle_interface_{self.__name}_{os.getpid()}", use_global_arguments=False, **kwargs)
        self.__executor = SingleThreadedExecutor()
        self.__executor.add_node(self.__node)
        self.__queue = SimpleQueue()
        self.__client = self.__node.create_client(GetPositionData, 
                                           f"data/get_{self.__name}",
                                           callback_group=MutuallyExclusiveCallbackGroup())
        self.__request = GetPositionData.Request()
        self.__thread = Thread(target=self.__spin)
        self.__thread.start()

    def __del__(self):
        self.__executor.shutdown()
        self.__thread.join()

    def __spin(self):
        while rclpy.ok():
            try:
                self.__executor.spin_once(timeout_sec=0.1)
            except Exception as e:
                return
    
    @property
    def name(self) -> str:
        return self.__name

    @property
    def ready(self) -> bool:
        return self.__client.service_is_ready()

    def wait_manager(self, timeout: Optional[float] = None) -> bool:
        self.__node.get_logger().debug(f"Data {self.__name} wait for manager server")
        return self.__client.wait_for_service(timeout_sec=timeout)

    def __nonzero__(self):
        return self.ready()

    def __bool__(self):
        return self.ready()

    def __done_cb(self, future):
        self.__node.get_logger().debug("data received")
        self.__queue.put(future.result().data)

    def get(self) -> GetPositionData.Response:
        future = self.__client.call_async(self.__request)
        future.add_done_callback(self.__done_cb)
        self.__node.get_logger().debug(f"get data {self.__name}")
        data = self.__queue.get()
        self.__node.get_logger().debug(f"-- data: {data}")
        return data
        
    def __call__(self):
        return self.get()

