import rclpy

from .data_interface import *


class DataSet:
    def __init__(self, **kwargs):
        self.__data = []
        
        self.__position = PositionData(**kwargs)
        self.__data.append(self.__position)

    
    @property
    def position(self) -> 'PositionData':
        return self.__position
    

    def __getitem__(self, item):
        return getattr(self, item)

    def __iter__(self):
        return iter(self.__data)

    def __len__(self):
        return len(self.__data)

    def __nonzero__(self):
        return len(self.__data) > 0
