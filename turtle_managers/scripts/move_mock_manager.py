#!/usr/bin/env python3
import rclpy

from turtle_managers.move_abstract_mock import AbstractMoveSkillMockManager

class MoveSkillMockManager(AbstractMoveSkillMockManager):

    def __init__(self):
        AbstractMoveSkillMockManager.__init__(self)


''' Don't change the main function unless you know what you do '''
def main(args=None):
    rclpy.init(args=args)
    server = MoveSkillMockManager()
    server.get_logger().info("Manager created")
    server.spin()

if __name__ == '__main__':
    main()
