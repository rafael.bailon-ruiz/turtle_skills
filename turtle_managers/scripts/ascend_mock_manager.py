#!/usr/bin/env python3
import rclpy

from turtle_managers.ascend_abstract_mock import AbstractAscendSkillMockManager

class AscendSkillMockManager(AbstractAscendSkillMockManager):

    def __init__(self):
        AbstractAscendSkillMockManager.__init__(self)


''' Don't change the main function unless you know what you do '''
def main(args=None):
    rclpy.init(args=args)
    server = AscendSkillMockManager()
    server.get_logger().info("Manager created")
    server.spin()

if __name__ == '__main__':
    main()
