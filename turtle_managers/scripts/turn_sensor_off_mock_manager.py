#!/usr/bin/env python3
import rclpy

from turtle_managers.turn_sensor_off_abstract_mock import AbstractTurnSensorOffSkillMockManager

class TurnSensorOffSkillMockManager(AbstractTurnSensorOffSkillMockManager):

    def __init__(self):
        AbstractTurnSensorOffSkillMockManager.__init__(self)


''' Don't change the main function unless you know what you do '''
def main(args=None):
    rclpy.init(args=args)
    server = TurnSensorOffSkillMockManager()
    server.get_logger().info("Manager created")
    server.spin()

if __name__ == '__main__':
    main()
