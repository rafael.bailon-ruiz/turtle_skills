#!/usr/bin/env python3
import rclpy

from turtle_managers.turn_sensor_off_abstract_manager import AbstractTurnSensorOffSkillManager

from functools import partial
from turtlesim.srv import SetPen

class TurnSensorOffSkillManager(AbstractTurnSensorOffSkillManager):

    def __init__(self):
        super().__init__()
        self.__client = self.create_client(SetPen, 'set_pen')
    
    def validate(self, goal):
        return self.__client.wait_for_service(timeout_sec=1.0)

    def on_dispatch(self, goal_id, goal):
        self.__future = self.__client.call_async(SetPen.Request(off=1))
        self.get_logger().info("Turning sensor OFF...")
        self.__future.add_done_callback(partial(self.__done_cb, goal_id))

    def __done_cb(self, goal_id, future):
        self.get_logger().info("Sensor OFF!")
        self.terminated_OFF(goal_id)

    def on_interrupt(self, goal_id, goal):
        self.terminated_OFF(goal_id)

    def progress(self, goal_id, goal):
        return .5
    
    def on_effect_sensor_off(self):
        pass
    

''' Don't change the main function unless you know what you do '''
def main(args=None):
    rclpy.init(args=args)
    server = TurnSensorOffSkillManager()
    server.get_logger().info("Manager created")
    server.spin()

if __name__ == '__main__':
    main()
