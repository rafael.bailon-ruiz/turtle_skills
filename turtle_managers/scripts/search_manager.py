#!/usr/bin/env python3
import rclpy

from turtle_managers.search_abstract_manager import AbstractSearchSkillManager

from turtlesim.msg import Color
from std_srvs.srv import SetBool
from functools import partial
from rclpy.time import Duration

class SearchSkillManager(AbstractSearchSkillManager):

    def __init__(self):
        super().__init__()
        self.create_subscription(Color, 'color_sensor', self.__color_cb, 100)
        self.declare_parameters('usbl_color', [('b', 0), ('g', 150), ('r', 0), ('tolerance', 10)])
        self.__client = self.create_client(SetBool, 'square_controller/activate')
        self.__timer = None
        self.__captured = False
        self.__init_time = self.get_clock().now()
        self.__progress = 0.0
    
    def __color_cb(self, msg):
        self.get_logger().debug(f"sensed color: {msg}")
        self.__captured = self.__captured or self.__usbl_captured(msg)

    def validate(self, goal):
        return self.__client.wait_for_service(timeout_sec=1.0)

    def __usbl_captured(self, color):
        tolerance = self.get_parameter("usbl_color.tolerance").value
        if abs(color.b - self.get_parameter("usbl_color.b").value) > tolerance:
            return False
        if abs(color.g - self.get_parameter("usbl_color.g").value) > tolerance:
            return False
        if abs(color.r - self.get_parameter("usbl_color.r").value) > tolerance:
            return False
        
        return True

    def __timer_cb(self, goal_id):
        now = self.get_clock().now()
        if self.__captured:
            self.get_logger().info("USBL color sensed!")
            self.__stop()
            self.terminated_FOUND(goal_id)
        
        self.get_logger().info(f"timed ellapsed: {(now - self.__init_time).nanoseconds}")
        self.get_logger().info(f"timeout: {self.__timeout.nanoseconds}")
        if (now - self.__init_time).nanoseconds > self.__timeout.nanoseconds:
            self.get_logger().warning(f"Timed out at {now}")
            self.__stop()
            self.terminated_TIMEDOUT(goal_id)

    def on_dispatch(self, goal_id, goal):
        self.__timeout = Duration(seconds=goal.timeout)
        self.__init_time = self.get_clock().now()
        self.get_logger().info(f"Start SEARCH at {self.__init_time}")
        self.__captured = False
        self.__client.call_async(SetBool.Request(data=True))
        self.__timer = self.create_timer(0.1, partial(self.__timer_cb, goal_id))

    def __stop(self):
        self.__client.call_async(SetBool.Request(data=False))
        if self.__timer is not None: self.__timer.cancel()

    def on_interrupt(self, goal_id, goal):
        self.__stop()
        self.terminated_TIMEDOUT(goal_id)

    def progress(self, goal_id, goal):
        now = self.get_clock().now()
        return (now - self.__init_time).nanoseconds / self.__timeout.nanoseconds
    
    def on_effect_moving(self):
        pass

    def on_effect_idle(self):
        pass
    

''' Don't change the main function unless you know what you do '''
def main(args=None):
    rclpy.init(args=args)
    server = SearchSkillManager()
    server.get_logger().info("Manager created")
    server.spin()

if __name__ == '__main__':
    main()
