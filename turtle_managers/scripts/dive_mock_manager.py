#!/usr/bin/env python3
import rclpy

from turtle_managers.dive_abstract_mock import AbstractDiveSkillMockManager

class DiveSkillMockManager(AbstractDiveSkillMockManager):

    def __init__(self):
        AbstractDiveSkillMockManager.__init__(self)


''' Don't change the main function unless you know what you do '''
def main(args=None):
    rclpy.init(args=args)
    server = DiveSkillMockManager()
    server.get_logger().info("Manager created")
    server.spin()

if __name__ == '__main__':
    main()
