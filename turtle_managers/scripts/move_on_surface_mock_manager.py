#!/usr/bin/env python3
import rclpy

from turtle_managers.move_on_surface_abstract_mock import AbstractMoveOnSurfaceSkillMockManager

class MoveOnSurfaceSkillMockManager(AbstractMoveOnSurfaceSkillMockManager):

    def __init__(self):
        AbstractMoveOnSurfaceSkillMockManager.__init__(self)


''' Don't change the main function unless you know what you do '''
def main(args=None):
    rclpy.init(args=args)
    server = MoveOnSurfaceSkillMockManager()
    server.get_logger().info("Manager created")
    server.spin()

if __name__ == '__main__':
    main()
