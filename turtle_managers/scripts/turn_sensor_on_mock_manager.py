#!/usr/bin/env python3
import rclpy

from turtle_managers.turn_sensor_on_abstract_mock import AbstractTurnSensorOnSkillMockManager

class TurnSensorOnSkillMockManager(AbstractTurnSensorOnSkillMockManager):

    def __init__(self):
        AbstractTurnSensorOnSkillMockManager.__init__(self)


''' Don't change the main function unless you know what you do '''
def main(args=None):
    rclpy.init(args=args)
    server = TurnSensorOnSkillMockManager()
    server.get_logger().info("Manager created")
    server.spin()

if __name__ == '__main__':
    main()
