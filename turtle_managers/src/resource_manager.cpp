#include <mutex>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_srvs/srv/empty.hpp"
#include "turtle_managers/srv/state.hpp"
#include "turtle_managers/srv/transition.hpp"

class TurtleResourceManager : public rclcpp::Node {
private:
  // Mutex
  std::mutex mutex_;
  rclcpp::Service<std_srvs::srv::Empty>::SharedPtr mutex_lock_srv_;
  rclcpp::Service<std_srvs::srv::Empty>::SharedPtr mutex_unlock_srv_;
  rclcpp::callback_group::CallbackGroup::SharedPtr mutex_group_;
  // Resources
  std_msgs::msg::String state_msg;
  
  std::string turtle_status_state_;
  rclcpp::Service<turtle_managers::srv::State>::SharedPtr turtle_status_get_srv_;
  rclcpp::Service<turtle_managers::srv::Transition>::SharedPtr turtle_status_change_srv_;
  rclcpp::Service<turtle_managers::srv::Transition>::SharedPtr turtle_status_change_internal_srv_;
  rclcpp::Publisher<std_msgs::msg::String>::SharedPtr turtle_status_state_publisher_;
  
  std::string sensor_state_;
  rclcpp::Service<turtle_managers::srv::State>::SharedPtr sensor_get_srv_;
  rclcpp::Service<turtle_managers::srv::Transition>::SharedPtr sensor_change_srv_;
  rclcpp::Service<turtle_managers::srv::Transition>::SharedPtr sensor_change_internal_srv_;
  rclcpp::Publisher<std_msgs::msg::String>::SharedPtr sensor_state_publisher_;
  
  std::string autonomous_control_state_;
  rclcpp::Service<turtle_managers::srv::State>::SharedPtr autonomous_control_get_srv_;
  rclcpp::Service<turtle_managers::srv::Transition>::SharedPtr autonomous_control_change_srv_;
  rclcpp::Service<turtle_managers::srv::Transition>::SharedPtr autonomous_control_change_internal_srv_;
  rclcpp::Publisher<std_msgs::msg::String>::SharedPtr autonomous_control_state_publisher_;
  
  std::string control_mode_state_;
  rclcpp::Service<turtle_managers::srv::State>::SharedPtr control_mode_get_srv_;
  rclcpp::Service<turtle_managers::srv::Transition>::SharedPtr control_mode_change_srv_;
  rclcpp::Service<turtle_managers::srv::Transition>::SharedPtr control_mode_change_internal_srv_;
  rclcpp::Publisher<std_msgs::msg::String>::SharedPtr control_mode_state_publisher_;
  

  
  bool turtle_status_transition_exists(std::string target, bool use_extern) {
    (void)use_extern; // remove unused warning
    if (this->turtle_status_state_ == "ON_SURFACE" && target == "IN_WATER")
      return true;
    if (this->turtle_status_state_ == "IN_WATER" && target == "ON_SURFACE")
      return true;
    return false;
  };

  bool turtle_status_transition(std::string target, bool use_extern)
  {
    RCLCPP_INFO(this->get_logger(), "received change request on turtle_status to %s", target.c_str());
    if (this->turtle_status_transition_exists(target, use_extern)) {
      this->turtle_status_state_ = target;
      this->state_msg.data = this->turtle_status_state_;
      this->turtle_status_state_publisher_->publish(this->state_msg);
      return true;
    }
    else {
      return false;
    }
  }

  void change_turtle_status_state(const std::shared_ptr<turtle_managers::srv::Transition::Request> request,
    std::shared_ptr<turtle_managers::srv::Transition::Response> response)
  {
    this->mutex_.lock();
    response->success = this->turtle_status_transition(request->target, true);
    response->state = this->turtle_status_state_;
    this->mutex_.unlock();
  }

  void change_turtle_status_internal_state(const std::shared_ptr<turtle_managers::srv::Transition::Request> request,
    std::shared_ptr<turtle_managers::srv::Transition::Response> response)
  {
    response->success = this->turtle_status_transition(request->target, false);
    response->state = this->turtle_status_state_;
  }

  void get_turtle_status_state(const std::shared_ptr<turtle_managers::srv::State::Request> request,
    std::shared_ptr<turtle_managers::srv::State::Response> response)
  {
    RCLCPP_INFO(this->get_logger(), "get_state of turtle_status: %s", this->turtle_status_state_.c_str());
    (void)request; // Remove 'unused parameter' warning
    response->state = this->turtle_status_state_;
  }
  
  bool sensor_transition_exists(std::string target, bool use_extern) {
    (void)use_extern; // remove unused warning
    if (this->sensor_state_ == "OFF" && target == "ON")
      return true;
    if (this->sensor_state_ == "ON" && target == "OFF")
      return true;
    if (this->sensor_state_ == "ON" && target == "OUT_OF_SERVICE")
      return use_extern;
    if (this->sensor_state_ == "OFF" && target == "OUT_OF_SERVICE")
      return use_extern;
    return false;
  };

  bool sensor_transition(std::string target, bool use_extern)
  {
    RCLCPP_INFO(this->get_logger(), "received change request on sensor to %s", target.c_str());
    if (this->sensor_transition_exists(target, use_extern)) {
      this->sensor_state_ = target;
      this->state_msg.data = this->sensor_state_;
      this->sensor_state_publisher_->publish(this->state_msg);
      return true;
    }
    else {
      return false;
    }
  }

  void change_sensor_state(const std::shared_ptr<turtle_managers::srv::Transition::Request> request,
    std::shared_ptr<turtle_managers::srv::Transition::Response> response)
  {
    this->mutex_.lock();
    response->success = this->sensor_transition(request->target, true);
    response->state = this->sensor_state_;
    this->mutex_.unlock();
  }

  void change_sensor_internal_state(const std::shared_ptr<turtle_managers::srv::Transition::Request> request,
    std::shared_ptr<turtle_managers::srv::Transition::Response> response)
  {
    response->success = this->sensor_transition(request->target, false);
    response->state = this->sensor_state_;
  }

  void get_sensor_state(const std::shared_ptr<turtle_managers::srv::State::Request> request,
    std::shared_ptr<turtle_managers::srv::State::Response> response)
  {
    RCLCPP_INFO(this->get_logger(), "get_state of sensor: %s", this->sensor_state_.c_str());
    (void)request; // Remove 'unused parameter' warning
    response->state = this->sensor_state_;
  }
  
  bool autonomous_control_transition_exists(std::string target, bool use_extern) {
    (void)use_extern; // remove unused warning
    if (this->autonomous_control_state_ == "ON" && target == "OFF")
      return use_extern;
    if (this->autonomous_control_state_ == "OFF" && target == "ON")
      return use_extern;
    return false;
  };

  bool autonomous_control_transition(std::string target, bool use_extern)
  {
    RCLCPP_INFO(this->get_logger(), "received change request on autonomous_control to %s", target.c_str());
    if (this->autonomous_control_transition_exists(target, use_extern)) {
      this->autonomous_control_state_ = target;
      this->state_msg.data = this->autonomous_control_state_;
      this->autonomous_control_state_publisher_->publish(this->state_msg);
      return true;
    }
    else {
      return false;
    }
  }

  void change_autonomous_control_state(const std::shared_ptr<turtle_managers::srv::Transition::Request> request,
    std::shared_ptr<turtle_managers::srv::Transition::Response> response)
  {
    this->mutex_.lock();
    response->success = this->autonomous_control_transition(request->target, true);
    response->state = this->autonomous_control_state_;
    this->mutex_.unlock();
  }

  void change_autonomous_control_internal_state(const std::shared_ptr<turtle_managers::srv::Transition::Request> request,
    std::shared_ptr<turtle_managers::srv::Transition::Response> response)
  {
    response->success = this->autonomous_control_transition(request->target, false);
    response->state = this->autonomous_control_state_;
  }

  void get_autonomous_control_state(const std::shared_ptr<turtle_managers::srv::State::Request> request,
    std::shared_ptr<turtle_managers::srv::State::Response> response)
  {
    RCLCPP_INFO(this->get_logger(), "get_state of autonomous_control: %s", this->autonomous_control_state_.c_str());
    (void)request; // Remove 'unused parameter' warning
    response->state = this->autonomous_control_state_;
  }
  
  bool control_mode_transition_exists(std::string target, bool use_extern) {
    (void)use_extern; // remove unused warning
    if (this->control_mode_state_ == "IDLE" && target == "MOVING")
      return true;
    if (this->control_mode_state_ == "MOVING" && target == "IDLE")
      return true;
    return false;
  };

  bool control_mode_transition(std::string target, bool use_extern)
  {
    RCLCPP_INFO(this->get_logger(), "received change request on control_mode to %s", target.c_str());
    if (this->control_mode_transition_exists(target, use_extern)) {
      this->control_mode_state_ = target;
      this->state_msg.data = this->control_mode_state_;
      this->control_mode_state_publisher_->publish(this->state_msg);
      return true;
    }
    else {
      return false;
    }
  }

  void change_control_mode_state(const std::shared_ptr<turtle_managers::srv::Transition::Request> request,
    std::shared_ptr<turtle_managers::srv::Transition::Response> response)
  {
    this->mutex_.lock();
    response->success = this->control_mode_transition(request->target, true);
    response->state = this->control_mode_state_;
    this->mutex_.unlock();
  }

  void change_control_mode_internal_state(const std::shared_ptr<turtle_managers::srv::Transition::Request> request,
    std::shared_ptr<turtle_managers::srv::Transition::Response> response)
  {
    response->success = this->control_mode_transition(request->target, false);
    response->state = this->control_mode_state_;
  }

  void get_control_mode_state(const std::shared_ptr<turtle_managers::srv::State::Request> request,
    std::shared_ptr<turtle_managers::srv::State::Response> response)
  {
    RCLCPP_INFO(this->get_logger(), "get_state of control_mode: %s", this->control_mode_state_.c_str());
    (void)request; // Remove 'unused parameter' warning
    response->state = this->control_mode_state_;
  }
  

  void mutex_lock(const std::shared_ptr<std_srvs::srv::Empty::Request> request,
    std::shared_ptr<std_srvs::srv::Empty::Response> response)
  {
    RCLCPP_INFO(this->get_logger(), "lock resource manager");
    (void)request; (void)response; // Remove 'unused parameter' warning
    this->mutex_.lock();
    RCLCPP_INFO(this->get_logger(), "resource manager locked");
  }

  void mutex_unlock(const std::shared_ptr<std_srvs::srv::Empty::Request> request,
    std::shared_ptr<std_srvs::srv::Empty::Response> response)
  {
    RCLCPP_INFO(this->get_logger(), "unlock resource manager");
    (void)request; (void)response; // Remove 'unused parameter' warning
    this->mutex_.unlock();
  }

public:
  explicit TurtleResourceManager()
    : rclcpp::Node("resource_manager")
  {
    using namespace std::placeholders;
    this->mutex_group_ = this->create_callback_group(rclcpp::callback_group::CallbackGroupType::Reentrant);
    this->mutex_lock_srv_ = this->create_service<std_srvs::srv::Empty>("resource/internal/lock",
      std::bind(&TurtleResourceManager::mutex_lock, this, _1, _2),
      rmw_qos_profile_services_default, this->mutex_group_);
    this->mutex_unlock_srv_ = this->create_service<std_srvs::srv::Empty>("resource/internal/unlock",
      std::bind(&TurtleResourceManager::mutex_unlock, this, _1, _2),
      rmw_qos_profile_services_default, this->mutex_group_);
    
    
    this->turtle_status_state_publisher_ = this->create_publisher<std_msgs::msg::String>("resource/turtle_status_state", 1);
    this->turtle_status_get_srv_ = this->create_service<turtle_managers::srv::State>("resource/get_turtle_status",
      std::bind(&TurtleResourceManager::get_turtle_status_state, this, _1, _2));
    this->turtle_status_change_srv_ = this->create_service<turtle_managers::srv::Transition>("resource/change_turtle_status",
      std::bind(&TurtleResourceManager::change_turtle_status_state, this, _1, _2));
    this->turtle_status_change_internal_srv_ = this->create_service<turtle_managers::srv::Transition>("resource/internal/change_turtle_status",
      std::bind(&TurtleResourceManager::change_turtle_status_internal_state, this, _1, _2));

    this->turtle_status_state_ = this->declare_parameter<std::string>("turtle_status_initial", "ON_SURFACE");
    RCLCPP_INFO(this->get_logger(), "Initialize resource turtle_status in state %s", this->turtle_status_state_.c_str());
    
    this->sensor_state_publisher_ = this->create_publisher<std_msgs::msg::String>("resource/sensor_state", 1);
    this->sensor_get_srv_ = this->create_service<turtle_managers::srv::State>("resource/get_sensor",
      std::bind(&TurtleResourceManager::get_sensor_state, this, _1, _2));
    this->sensor_change_srv_ = this->create_service<turtle_managers::srv::Transition>("resource/change_sensor",
      std::bind(&TurtleResourceManager::change_sensor_state, this, _1, _2));
    this->sensor_change_internal_srv_ = this->create_service<turtle_managers::srv::Transition>("resource/internal/change_sensor",
      std::bind(&TurtleResourceManager::change_sensor_internal_state, this, _1, _2));

    this->sensor_state_ = this->declare_parameter<std::string>("sensor_initial", "OFF");
    RCLCPP_INFO(this->get_logger(), "Initialize resource sensor in state %s", this->sensor_state_.c_str());
    
    this->autonomous_control_state_publisher_ = this->create_publisher<std_msgs::msg::String>("resource/autonomous_control_state", 1);
    this->autonomous_control_get_srv_ = this->create_service<turtle_managers::srv::State>("resource/get_autonomous_control",
      std::bind(&TurtleResourceManager::get_autonomous_control_state, this, _1, _2));
    this->autonomous_control_change_srv_ = this->create_service<turtle_managers::srv::Transition>("resource/change_autonomous_control",
      std::bind(&TurtleResourceManager::change_autonomous_control_state, this, _1, _2));
    this->autonomous_control_change_internal_srv_ = this->create_service<turtle_managers::srv::Transition>("resource/internal/change_autonomous_control",
      std::bind(&TurtleResourceManager::change_autonomous_control_internal_state, this, _1, _2));

    this->autonomous_control_state_ = this->declare_parameter<std::string>("autonomous_control_initial", "ON");
    RCLCPP_INFO(this->get_logger(), "Initialize resource autonomous_control in state %s", this->autonomous_control_state_.c_str());
    
    this->control_mode_state_publisher_ = this->create_publisher<std_msgs::msg::String>("resource/control_mode_state", 1);
    this->control_mode_get_srv_ = this->create_service<turtle_managers::srv::State>("resource/get_control_mode",
      std::bind(&TurtleResourceManager::get_control_mode_state, this, _1, _2));
    this->control_mode_change_srv_ = this->create_service<turtle_managers::srv::Transition>("resource/change_control_mode",
      std::bind(&TurtleResourceManager::change_control_mode_state, this, _1, _2));
    this->control_mode_change_internal_srv_ = this->create_service<turtle_managers::srv::Transition>("resource/internal/change_control_mode",
      std::bind(&TurtleResourceManager::change_control_mode_internal_state, this, _1, _2));

    this->control_mode_state_ = this->declare_parameter<std::string>("control_mode_initial", "IDLE");
    RCLCPP_INFO(this->get_logger(), "Initialize resource control_mode in state %s", this->control_mode_state_.c_str());
    
    RCLCPP_INFO(this->get_logger(), "Resource Manager ready");
  }

};

int main(int argc, char **argv)
{
  rclcpp::init(argc, argv);
  auto rm = std::make_shared<TurtleResourceManager>();
  rm->declare_parameter("spinner_threads", 7+2);
  auto spinner_threads = rm->get_parameter("spinner_threads").as_int();
  RCLCPP_INFO(rm->get_logger(), "start manager with %d threads", spinner_threads);
  rclcpp::executors::MultiThreadedExecutor executor(rclcpp::executor::ExecutorArgs(), spinner_threads);
  executor.add_node(rm);
  executor.spin();
  rclcpp::shutdown();
  return 0;
}
