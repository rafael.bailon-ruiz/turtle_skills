from abc import ABC, abstractmethod
from functools import partial
import enum
import threading
import math

import rclpy
from rclpy.callback_groups import ReentrantCallbackGroup, MutuallyExclusiveCallbackGroup
from rclpy.executors import MultiThreadedExecutor
from rclpy.node import Node
from rclpy.action import ActionServer
from rclpy.action.server import GoalResponse, GoalStatus, CancelResponse

from std_msgs.msg import String
from std_srvs.srv import Empty
from diagnostic_msgs.srv import SelfTest
from diagnostic_msgs.msg import DiagnosticStatus, KeyValue

from turtle_managers.action import MoveOnSurfaceSkill
from turtle_managers.srv import State, Transition
from .logic import *

@enum.unique
class SkillMove_on_surfaceGoalState(enum.Enum):
    S = 'Started'
    NV = 'Not Valid'
    CR = 'Checking Resources'
    NR = 'Resources Preconditions Not Matched'
    RG = 'Running'
    IG = 'Interrupting'
    RI = 'Resource Invariant Violated'
    T_ARRIVED = 'Terminating ARRIVED'
    M_ARRIVED = 'Terminated in ARRIVED'
    N_ARRIVED = 'Postcondition failure in ARRIVED'
    T_ABORTED = 'Terminating ABORTED'
    M_ABORTED = 'Terminated in ABORTED'
    N_ABORTED = 'Postcondition failure in ABORTED'

    @classmethod
    def is_terminal(cls, state):
        if state == cls.NV: return True
        if state == cls.NR: return True
        if state == cls.RI: return True
        if state == cls.M_ARRIVED: return True
        if state == cls.N_ARRIVED: return True
        if state == cls.M_ABORTED: return True
        if state == cls.N_ABORTED: return True
        return False

class AbstractMoveOnSurfaceSkillManager(ABC, Node):

    def __init__(self):
        Node.__init__(self, 'move_on_surface_manager')
        self.__action_callback_group = ReentrantCallbackGroup()
        self.__resource_callback_group = ReentrantCallbackGroup()
        self.__executor = MultiThreadedExecutor()

        self.__goals = dict()
        self.__goals_msgs = dict()
        self.__goals_states = dict()
        self.__goals_progress = dict()
        self.__goals_start_time = dict()
        self.__goals_end_time = dict()
        self.__modes = dict()
        self.__events = dict()

        self.__as = ActionServer(self,
            MoveOnSurfaceSkill,
            'skill/move_on_surface',
            self.__exec_cb,
            goal_callback=self.__goal_cb,
            handle_accepted_callback=self.__accepted_cb,
            cancel_callback=self.__cancel_cb,
            callback_group=self.__action_callback_group
            )

        
        self.__resource_lock = self.create_client(Empty,
            'resource/internal/lock',
            callback_group=self.__resource_callback_group)
        self.__resource_unlock = self.create_client(Empty,
            'resource/internal/unlock',
            callback_group=self.__resource_callback_group)
        self.get_logger().info("waiting connection to resource manager")
        self.__resource_lock.wait_for_service()
        self.__resource_transition = dict()
        self.__resource_state = dict()
        self.__resource_sub = dict()
        
        self.get_logger().info("create client for resource manager autonomous_control")
        self.__resource_transition['autonomous_control'] = self.create_client(Transition,
            'resource/internal/change_autonomous_control',
            callback_group=self.__resource_callback_group)
        self.__resource_state['autonomous_control'] = self.create_client(State,
            'resource/get_autonomous_control',
            callback_group=self.__resource_callback_group)
        self.__resource_sub['autonomous_control'] = self.create_subscription(String,
            'resource/autonomous_control_state',
            self.__invariants_cb,
            10)
        
        self.get_logger().info("create client for resource manager control_mode")
        self.__resource_transition['control_mode'] = self.create_client(Transition,
            'resource/internal/change_control_mode',
            callback_group=self.__resource_callback_group)
        self.__resource_state['control_mode'] = self.create_client(State,
            'resource/get_control_mode',
            callback_group=self.__resource_callback_group)
        self.__resource_sub['control_mode'] = self.create_subscription(String,
            'resource/control_mode_state',
            self.__invariants_cb,
            10)
        
        self.get_logger().info("create client for resource manager robot_status")
        self.__resource_transition['robot_status'] = self.create_client(Transition,
            'resource/internal/change_robot_status',
            callback_group=self.__resource_callback_group)
        self.__resource_state['robot_status'] = self.create_client(State,
            'resource/get_robot_status',
            callback_group=self.__resource_callback_group)
        self.__resource_sub['robot_status'] = self.create_subscription(String,
            'resource/robot_status_state',
            self.__invariants_cb,
            10)
        
        self.get_logger().info("create client for resource manager turtle_status")
        self.__resource_transition['turtle_status'] = self.create_client(Transition,
            'resource/internal/change_turtle_status',
            callback_group=self.__resource_callback_group)
        self.__resource_state['turtle_status'] = self.create_client(State,
            'resource/get_turtle_status',
            callback_group=self.__resource_callback_group)
        self.__resource_sub['turtle_status'] = self.create_subscription(String,
            'resource/turtle_status_state',
            self.__invariants_cb,
            10)
        
        

        self.__formula_pre = dict()
        
        self.__formula_pre['on_surface'] = And(Equals('turtle_status', 'ON_SURFACE'), Equals('control_mode', 'IDLE'))
        
        self.__formula_pre['autonomous'] = Equals('autonomous_control', 'ON')
        
        self.__formula_modes = dict()
        
        self.__formula_inv = dict()
        
        self.__formula_inv['on_surface'] = Equals('robot_status', 'ON_SURFACE')
        
        self.__formula_inv['autonomous'] = Equals('autonomous_control', 'ON')
        

        self.__status_service = self.create_service(SelfTest,
            'skill/move_on_surface/get_status',
            self.get_status,
            callback_group=self.__action_callback_group)

        self.__progress_period = 1.0
        
        self.__timer = self.create_timer(1.0, self.publish_feedbacks,
                                         callback_group=self.__action_callback_group)
        
        self.get_logger().info("skill manager started")

    def __id(self, gh):
        return str(gh.goal_id.uuid)

    @property
    def progress_period(self):
        return self.__progress_period

    def spin(self):
        rclpy.spin(self, self.__executor)

    def spin_until_future_complete(self, future):
        return self.__executor.spin_until_future_complete(future)

    def get_status(self, request, response):
        response.status = []
        response.status.append(DiagnosticStatus(level=DiagnosticStatus.OK,
            name='move_on_surface',
            message='turtle_managers.action.MoveOnSurfaceSkill'))
        for gid, st in self.__goals_states.items():
            status = DiagnosticStatus(name='move_on_surface')
            if gid in self.__goals:
                status.level = DiagnosticStatus.OK
                status.values.append(KeyValue(key='server_state', value=str(self.__goals[gid].status)))
                #status.values.append(KeyValue(key='server_state_text', value=self.__goals[gid].get_goal_status().text))
            else:
                status.level = DiagnosticStatus.STALE
            status.hardware_id = gid
            status.values.append(KeyValue(key='start_time', value=str(self.__goals_start_time[gid])))
            status.values.append(KeyValue(key='end_time', value=str(self.__goals_end_time[gid])))            
            status.values.append(KeyValue(key='goal', value=repr(self.__goals_msgs[gid])))
            status.values.append(KeyValue(key='skill_state', value=st.name))
            status.values.append(KeyValue(key='skill_state_text', value=st.value))
            status.values.append(KeyValue(key='progress', value=str(self.__goals_progress[gid])))
            response.status.append(status)
        response.id = 'move_on_surface'
        response.passed = DiagnosticStatus.OK
        return response

    def check_pre(self):
        for label, formula in self.__formula_pre.items():
            states = dict()
            for res in formula.symbols():
                future = self.__resource_state[res].call_async(State.Request())
                self.__executor.spin_until_future_complete(future)
                self.get_logger().debug("received response from resource manager")
                states[res] = future.result().state
            if not formula.eval(states):
                self.get_logger().error("precondition {} error".format(label))
                return False, label
        return True, ''

    
    def __effect_moving(self):
        self.on_effect_moving()
        
        future = self.__resource_transition['control_mode'].call_async(Transition.Request(
            target='MOVING'))
        self.__executor.spin_until_future_complete(future)
        self.get_logger().debug("received response from resource manager")
        r = future.result()
        if not r.success:
            self.get_logger().error("error changing resource control_mode state: {} -> MOVING".format(r.source))
            return False, 'control_mode'
        
        return True, ''
    
    def __effect_idle(self):
        self.on_effect_idle()
        
        future = self.__resource_transition['control_mode'].call_async(Transition.Request(
            target='IDLE'))
        self.__executor.spin_until_future_complete(future)
        self.get_logger().debug("received response from resource manager")
        r = future.result()
        if not r.success:
            self.get_logger().error("error changing resource control_mode state: {} -> IDLE".format(r.source))
            return False, 'control_mode'
        
        return True, ''
    

    
    def __resource_manager_lock(self):
        self.get_logger().info("locking resource manager")
        future = self.__resource_lock.call_async(Empty.Request())
        self.__executor.spin_until_future_complete(future)
        self.get_logger().debug("resource manager locked")
        
    def __resource_manager_unlock(self):
        self.get_logger().info("unlocking resource manager")
        future = self.__resource_unlock.call_async(Empty.Request())
        self.__executor.spin_until_future_complete(future)
        self.get_logger().debug("resource manager unlocked")
    

    def __invariants_cb(self, msg):
        active_goals = [gid for gid in self.__goals
                        if (self.__goals_states[gid] == SkillMove_on_surfaceGoalState.RG
                            or self.__goals_states[gid] == SkillMove_on_surfaceGoalState.IG)]
        if not active_goals:
            return

        
        self.__resource_manager_lock()
        
        
        states = dict()
        for res in self.__formula_inv['on_surface'].symbols():
            future = self.__resource_state[res].call_async(State.Request())
            self.__executor.spin_until_future_complete(future)
            self.get_logger().debug("received response from resource manager")
            states[res] = future.result().state

        if not self.__formula_inv['on_surface'].eval(states):
            self.get_logger().warn("invariant on_surface violated!")
            
            val, label = self.__effect_idle()
            if not val:
                self.get_logger().error("error applying effect idle in invariant on_surface")
            
            for gid in active_goals:
                gh = self.__goals[gid]
                self.__modes[gid] = "RI(on_surface)"
                gh.abort()
                self.__goals_states[gid] = SkillMove_on_surfaceGoalState.RI
                self.__events[gid].set()
            
            self.__resource_manager_unlock()
            
            return
        
        states = dict()
        for res in self.__formula_inv['autonomous'].symbols():
            future = self.__resource_state[res].call_async(State.Request())
            self.__executor.spin_until_future_complete(future)
            self.get_logger().debug("received response from resource manager")
            states[res] = future.result().state

        if not self.__formula_inv['autonomous'].eval(states):
            self.get_logger().warn("invariant autonomous violated!")
            
            val, label = self.__effect_idle()
            if not val:
                self.get_logger().error("error applying effect idle in invariant autonomous")
            
            for gid in active_goals:
                gh = self.__goals[gid]
                self.__modes[gid] = "RI(autonomous)"
                gh.abort()
                self.__goals_states[gid] = SkillMove_on_surfaceGoalState.RI
                self.__events[gid].set()
            
            self.__resource_manager_unlock()
            
            return
        
        
        self.__resource_manager_unlock()
        

    def __goal_cb(self, goal_request):
        self.get_logger().info("validating goal {}".format(goal_request))
        #self.__goals_states[gid] = SkillMove_on_surfaceGoalState.S

        if self.validate(goal_request):
            self.get_logger().info("goal {} validated".format(goal_request))
            #self.__goals_states[gid] = SkillMove_on_surfaceGoalState.CR
            return GoalResponse.ACCEPT
        else:
            self.get_logger().warn("goal {} not validated".format(goal_request))
            #self.__goals_states[gid] = SkillMove_on_surfaceGoalState.NV
            return GoalResponse.REJECT # terminal state NV

    def __accepted_cb(self, gh):
        gid = self.__id(gh)
        self.__goals[gid] = gh
        self.__goals_msgs[gid] = gh.request
        self.__goals_progress[gid] = 0.0
        self.__goals_states[gid] = SkillMove_on_surfaceGoalState.CR
        self.__goals_start_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
        self.__goals_end_time[gid] = math.inf
        
        self.__resource_manager_lock()
        val, res = self.check_pre()
        if not val:
            self.__resource_manager_unlock()
            self.get_logger().warn("error in precondition {}".format(res))
            self.__modes[gid] = "NR({})".format(res)
            gh.execute()
            gh.abort()
            self.__goals_states[gid] = SkillMove_on_surfaceGoalState.NR
            self.__goals_end_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
            return
        
        val, res = self.__effect_moving()
        if not val:
            self.__resource_manager_unlock()
            self.get_logger().warn("error in effect {}".format(res))
            self.__modes[gid] = "NR({})".format(res)
            gh.execute()
            gh.abort()
            self.__goals_states[gid] = SkillMove_on_surfaceGoalState.NR
            self.__goals_end_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
            return
        
        self.__resource_manager_unlock()
        
        self.__events[gid] = threading.Event()
        self.get_logger().info("dispatching goal {}".format(gh.goal_id))
        self.__goals_states[gid] = SkillMove_on_surfaceGoalState.RG
        self.__executor.create_task(self.on_dispatch, gid, gh.request)
        gh.execute()

    def __cancel_cb(self, gh):
        self.get_logger().info("interrupting goal {} {}".format(gh.goal_id, gh.request))
        self.__goals_states[self.__id(gh)] = SkillMove_on_surfaceGoalState.IG
        self.__executor.create_task(self.on_interrupt, self.__id(gh), gh.request)
        return CancelResponse.ACCEPT

    def __exec_cb(self, gh):
        gid = self.__id(gh)
        if gid not in self.__events:
            self.__goals_end_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
            return MoveOnSurfaceSkill.Result(mode='NR')
        self.__events[gid].wait()
        status = self.__goals_states[gid]
        if not SkillMove_on_surfaceGoalState.is_terminal(status):
            self.get_logger().error("[goal {}: execution callback finished with status {}".format(gh.status, gid))
        else:
            mode = self.__modes[gid]
            self.get_logger().info("goal {}: returning mode {}".format(gid, mode))
            self.__goals_end_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
            return MoveOnSurfaceSkill.Result(mode=mode)

    def publish_feedbacks(self):
        to_remove = []
        self.get_logger().debug("publishing progress")
        for gid, gh in self.__goals.items():
            status = self.__goals_states[gid]
            if status == SkillMove_on_surfaceGoalState.RG:
                progress = self.progress(gid, gh.request)
                self.__goals_progress[gid] = progress
                self.get_logger().debug(" - goal {} progress {}".format(gid, progress))
                gh.publish_feedback(MoveOnSurfaceSkill.Feedback(progress=progress))
            elif SkillMove_on_surfaceGoalState.is_terminal(status):
                to_remove.append(gid)
        for gid in to_remove:
            self.get_logger().info("removing terminated goal {}".format(gid))
            del self.__goals[gid]
            del self.__events[gid]

    
    def check_post_ARRIVED(self):
        
        return True
        

    def terminated_ARRIVED(self, gid):
        try:
            gh = self.__goals[gid]
        except KeyError:
            self.get_logger().warn("goal {} already purged".format(gid))
            return
        status = self.__goals_states[self.__id(gh)]
        if SkillMove_on_surfaceGoalState.is_terminal(status):
            self.get_logger().warn("goal {} already terminated in {}".format(gid, status))
            return
        
        if (status == SkillMove_on_surfaceGoalState.RG or status == SkillMove_on_surfaceGoalState.IG):
            self.get_logger().info("terminating goal {} {} in mode ARRIVED".format(gid, gh.request))
            self.__goals_states[gid] = SkillMove_on_surfaceGoalState.T_ARRIVED
            
            self.__resource_manager_lock()
            if not self.check_post_ARRIVED():
                self.get_logger().error("mode ARRIVED postcondition error")
                self.__modes[gid] = 'N_ARRIVED'
                self.__goals_states[gid] = SkillMove_on_surfaceGoalState.N_ARRIVED
                gh.abort()
                self.__events[gid].set()
                self.__resource_manager_unlock()
                return
            
            val, label = self.__effect_idle()
            if not val:
                self.get_logger().error("error applying effect idle in mode ARRIVED")
                self.__modes[gid] = 'N_ARRIVED'
                self.__goals_states[gid] = SkillMove_on_surfaceGoalState.N_ARRIVED
                gh.abort()
                self.__events[gid].set()
                self.__resource_manager_unlock()
                return
            
            self.__resource_manager_unlock()
            
            self.get_logger().info("goal {} succeeded in mode ARRIVED".format(gid))
            self.__modes[gid] = 'M_ARRIVED'
            self.__goals_states[gid] = SkillMove_on_surfaceGoalState.M_ARRIVED
            gh.succeed()
            self.__events[gid].set()
        else:
            self.get_logger().warn("goal {} not active ({})".format(gid, gh.status))
    
    def check_post_ABORTED(self):
        
        return True
        

    def terminated_ABORTED(self, gid):
        try:
            gh = self.__goals[gid]
        except KeyError:
            self.get_logger().warn("goal {} already purged".format(gid))
            return
        status = self.__goals_states[self.__id(gh)]
        if SkillMove_on_surfaceGoalState.is_terminal(status):
            self.get_logger().warn("goal {} already terminated in {}".format(gid, status))
            return
        
        if (status == SkillMove_on_surfaceGoalState.RG or status == SkillMove_on_surfaceGoalState.IG):
            self.get_logger().info("terminating goal {} {} in mode ABORTED".format(gid, gh.request))
            self.__goals_states[gid] = SkillMove_on_surfaceGoalState.T_ABORTED
            
            self.__resource_manager_lock()
            if not self.check_post_ABORTED():
                self.get_logger().error("mode ABORTED postcondition error")
                self.__modes[gid] = 'N_ABORTED'
                self.__goals_states[gid] = SkillMove_on_surfaceGoalState.N_ABORTED
                gh.abort()
                self.__events[gid].set()
                self.__resource_manager_unlock()
                return
            
            val, label = self.__effect_idle()
            if not val:
                self.get_logger().error("error applying effect idle in mode ABORTED")
                self.__modes[gid] = 'N_ABORTED'
                self.__goals_states[gid] = SkillMove_on_surfaceGoalState.N_ABORTED
                gh.abort()
                self.__events[gid].set()
                self.__resource_manager_unlock()
                return
            
            self.__resource_manager_unlock()
            
            self.get_logger().info("goal {} succeeded in mode ABORTED".format(gid))
            self.__modes[gid] = 'M_ABORTED'
            self.__goals_states[gid] = SkillMove_on_surfaceGoalState.M_ABORTED
            gh.succeed()
            self.__events[gid].set()
        else:
            self.get_logger().warn("goal {} not active ({})".format(gid, gh.status))
    

    ''' User functions must be reimplemented '''

    @abstractmethod
    def validate(self, goal_request):
        pass

    @abstractmethod
    def on_dispatch(self, goal_id, goal):
        pass

    @abstractmethod
    def on_interrupt(self, goal_id, goal):
        pass

    @abstractmethod
    def progress(self, goal_id, goal):
        pass

    
    @abstractmethod
    def on_effect_moving(self):
        pass
    
    @abstractmethod
    def on_effect_idle(self):
        pass
    
